# Este es el archivo Makefile de la práctica, editarlo como sea neceario
INCLUDES= ./include/
SOURCE= ./src/main.c
LIBS= ./lib/vector.c
CC = gcc
APART= -lm


# Target que se ejecutará por defecto con el comando make
all: dynamic

# Target que compilará el proyecto usando librerías estáticas
static: main.o libvector.a
	$(CC) -static -o ejecutable main.o ./libvector.a -I$(INCLUDES) $(APART)

main.o: $(SOURCE) $(INCLUDE)
	$(CC) -c $(SOURCE) -I$(INCLUDES)

libvector.a: vector.o
	ar rcs libvector.a vector.o

vector.o: $(LIBS) $(INCLUDES)vector.h
	$(CC) -c $(LIBS) -I$(INCLUDES)

# Target que compilará el proyecto usando librerías dinámicas
dynamic: $(SOURCE) libvector.so
	$(CC) -o ejecutable $(SOURCE) ./libvector.so -I$(INCLUDES) $(APART)

libvector.so: $(LIBS) $(INCLUDES)vector.h
	$(CC) -shared -fPIC -o libvector.so $(LIBS) -I$(INCLUDES)

# Limpiar archivos temporales
clean:
	rm -f *o *a ejecutable
