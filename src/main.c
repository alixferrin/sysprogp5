#include<stdio.h>
#include<vector.h>

int main() {
	vector3D a;
	vector3D b;
	float productoEscalar;
	vector3D productoCruz;
	float magnitud1;
	float magnitud2;
	int ortogonalidad;
	printf("Ingrese la componente x del vector 1: ");
	scanf("%f",&a.x);
	printf("Ingrese la componente y del vector 1: ");
	scanf("%f",&a.y);
	printf("Ingrese la componente z del vector 1: ");
	scanf("%f",&a.z);
	printf("Ingrese la componente x del vector 2: ");
	scanf("%f",&b.x);
	printf("Ingrese la componente y del vector 2: ");				
	scanf("%f",&b.y);							        
	printf("Ingrese la componente z del vector 2: ");
	scanf("%f",&b.z);
	productoEscalar = dotproduct(a,b);
	productoCruz = crossproduct(a,b);
	magnitud1 = magnitud(a);
	magnitud2 = magnitud(b);
	ortogonalidad = esOrtogonal(a,b);
	printf("Producto escalar= %f\n",productoEscalar);
	printf("Producto vectorial= V3 = (%f,%f,%f)\n",productoCruz.x,productoCruz.y,productoCruz.z);
	printf("Magnitud vector 1= %f\n",magnitud1);
	printf("Magnitud vector 2= %f\n",magnitud2);
	printf("Son ortogonales los vectores?[1=si,0=no]  %i\n",ortogonalidad);

}
