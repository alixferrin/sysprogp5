/* implementar aquí las funciones requeridas */
#include<stdio.h>
#include<vector.h>
#include<math.h>

float dotproduct(vector3D v1, vector3D v2){
float producto_escalar;
	producto_escalar = v1.x*v2.x + v1.y*v2.y + v1.z*v2.z;
	return producto_escalar;
}

vector3D crossproduct(vector3D v1, vector3D v2){
vector3D producto_vectorial;
float x1,y1,z1;
	x1 = v1.y*v2.z - v1.z*v2.y;
        y1 = v1.x*v2.z - v1.z*v2.x;
        z1 = v1.x*v2.y - v1.y*v2.x;
	producto_vectorial.x = x1;
	producto_vectorial.y = y1;
	producto_vectorial.z = z1;
	return producto_vectorial;
}


float magnitud(vector3D v){
float resultado, resultado1;
	resultado = pow(v.x,2) + pow(v.y,2) + pow(v.z,2);
	resultado1= sqrt(resultado);
	return resultado1;
}
	

int esOrtogonal(vector3D v1, vector3D v2){
 if (dotproduct(v1,v2)==0){
	return 1;
 }
else {
	return 0;
}
}


